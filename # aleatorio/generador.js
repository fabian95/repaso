class Generador{

    miNumero;
    x =document.getElementById("superior");
    y =document.getElementById("inferior");

    constructor(x,y){
        this.superior = x;
        this.inferior = y;
    }

    set setSuperior(valor){
        this.superior=valor;
        
    }

    set setInferior(valor){
        this.inferior=valor;
        
    }
    
    get getValor(){
        this.miNumero=this.generarNumero();
        return this.miNumero;
    }

    generarNumero(x,y) {
        
        let n;
        n=Math.random();
        n=n*(this.superior-this.inferior)+this.inferior;
        n=Math.round(n);
        //n = Math.round(Math.random() * 25 + + 1);
        return n;
    }
    
}
